﻿using CommandLineParser.Arguments;

namespace Ja.JoinHitBoxes
{
	class ParsingTarget
	{
		[ValueArgument(
			typeof(string),
			'l',
			"left",
			Description = "Input path to first json-file with hitbox coordinates",
			Optional = false
		)]
		public string Left;

		[ValueArgument(
			typeof(string),
			'r',
			"right",
			Description = "Input path to second json-file with hitbox coordinates",
			Optional = false
		)]
		public string Right;

		[ValueArgument(
			typeof(string),
			'o',
			"o",
			Description = "Output path to json-file with flipped hitbox coordinates",
			Optional = false
		)]
		public string Output;
	}
}
