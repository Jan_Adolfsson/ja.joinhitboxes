﻿using System.Collections.Generic;
using System.Drawing;

namespace Ja.JoinHitBoxes
{
	public class Tile
	{
		public Point Index { get; set; }
		public List<Polygon> Polygons { get; set; }
	}
}
