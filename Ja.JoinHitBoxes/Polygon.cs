﻿using System.Collections.Generic;
using System.Drawing;

namespace Ja.JoinHitBoxes
{
	public class Polygon
	{
		public List<Point> Vertexes { get; set; }
		public string Purpose { get; set; }
		public List<string> Tags { get; set; }
	}
}
