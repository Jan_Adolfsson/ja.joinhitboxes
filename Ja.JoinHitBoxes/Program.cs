﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLineParser.Exceptions;
using Newtonsoft.Json;

namespace Ja.JoinHitBoxes
{
	class Program
	{
		static void Main(string[] args)
		{
			ParsingTarget parsingTarget = new ParsingTarget();
			if (!ParseCommandLineInput(parsingTarget, args))
				return;

			if (!ReadInput(parsingTarget.Left, out List<Tile> leftTiles))
				return;

			if (!ReadInput(parsingTarget.Right, out List<Tile> rightTiles))
				return;

			var tilesJoined = JoinHitBoxes(leftTiles, rightTiles);

			WriteOutput(parsingTarget.Output, tilesJoined);
		}

		private static bool ParseCommandLineInput(ParsingTarget parsingTarget, string[] args)
		{
			CommandLineParser.CommandLineParser parser =
				new CommandLineParser.CommandLineParser();
			parser.ExtractArgumentAttributes(parsingTarget);

			try
			{
				parser.ParseCommandLine(args);
				return true;
			}
			catch (UnknownArgumentException uae)
			{
				Console.WriteLine($"Unknown argument {uae.Argument}");
			}
			catch (CommandLineFormatException clfe)
			{
				Console.WriteLine($"Invalid argument format {clfe.Message}");
			}
			catch (CommandLineArgumentOutOfRangeException claoore)
			{
				Console.WriteLine($"Invalid value {claoore.Message}");
			}
			catch (MandatoryArgumentNotSetException manse)
			{
				Console.WriteLine($"Mandatory argument not set {manse.Argument}");
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Unhandled exception.\r\n{ex}");
			}

			return false;
		}

		private static List<Tile> JoinHitBoxes(List<Tile> leftTiles, List<Tile> rightTiles)
		{
			List<Tile> tilesJoined = new List<Tile>();
			tilesJoined.AddRange(leftTiles);

			rightTiles.ForEach(t => t.Index = new Point(t.Index.X, t.Index.Y + 1));
			tilesJoined.AddRange(rightTiles);

			return tilesJoined;
		}

		private static bool ReadInput(string input, out List<Tile> tiles)
		{
			tiles = null;
			try
			{
				tiles = ReadInput(input);
				return true;
			}
			catch
			{
				Console.WriteLine($"Failed reading input {input}.");
				return false;
			}
		}
		private static List<Tile> ReadInput(string path)
		{
			using (StreamReader file = File.OpenText(path))
			{
				JsonSerializer serializer = new JsonSerializer();
				var tiles = (List<Tile>)serializer.Deserialize(file, typeof(List<Tile>));
				return tiles;
			}
		}
		private static void WriteOutput(string path, List<Tile> tiles)
		{
			try
			{
				using (StreamWriter file = File.CreateText(path))
				{
					JsonSerializer serializer = new JsonSerializer();
					serializer.Serialize(file, tiles);
				}
			}
			catch
			{
				Console.WriteLine("Failed writing output.");
			}
		}
	}
}
